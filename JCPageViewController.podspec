Pod::Spec.new do |s|
  s.name = 'JCPageViewController'
  s.version = '0.1'
  s.license = 'MIT'
  s.summary = 'Page View Controllers with ease.'
  # s.homepage = 'https://github.com/'
  # s.social_media_url = 'http://twitter.com/...'
  s.authors = { 'Josh Campion' => 'joshcampion89@gmail.com' }
  # s.source = { :git => 'git@bitbucket.org:josh_TD/thedistancecore.git', :tag => s.version }

  s.ios.deployment_target = '8.0'

  s.source_files = 'JCPageViewController/Classes/**/*.swift', 'JCPageViewController/Extensions/**/*.swift'
  s.requires_arc = true
  s.dependency 'TZStackView'
end