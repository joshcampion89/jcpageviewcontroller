//
//  UIView+JCExtensions.swift
//  Get Games
//
//  Created by Josh Campion on 04/01/2015.
//  Copyright (c) 2015 Josh Campion. All rights reserved.
//

import UIKit

extension UIView {
    
    // MARK: - Constraint Helper Methods
    
    /**
    
    Helper method returning the constraints to align a view (or subview) to {@code self} with the given insets.
    
    - parameter view: The view to align to self.
    - parameter insets: The insets to align the {@code view} parameter against.
    :return: An array containing the top, left, bottom, and right NSLayoutConstraints to align the {@code view} parameter to {@code self}.
    */
    func constraintsToAlignView(view: UIView, withInsets insets : UIEdgeInsets = UIEdgeInsetsZero) -> Array<NSLayoutConstraint> {
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        var constraints = Array<NSLayoutConstraint>()
        
        let topConstraint = NSLayoutConstraint(item: view,
            attribute: .Top,
            relatedBy: .Equal,
            toItem: self,
            attribute: .Top,
            multiplier: 1.0,
            constant: insets.top)
        
        let bottomConstraint = NSLayoutConstraint(item: view,
            attribute: .Bottom,
            relatedBy: .Equal,
            toItem: self,
            attribute: .Bottom,
            multiplier: 1.0,
            constant: (-insets.bottom))
        
        let leftConstraint = NSLayoutConstraint(item: view,
            attribute: .Left,
            relatedBy: .Equal,
            toItem: self,
            attribute: .Left,
            multiplier: 1.0,
            constant: insets.left)
        
        let rightConstraint = NSLayoutConstraint(item: view,
            attribute: .Right,
            relatedBy: .Equal,
            toItem: self,
            attribute: .Right,
            multiplier: 1.0,
            constant: (-insets.right))
        
        // append in the same order as UIEdgeInsets parameters.
        constraints.append(topConstraint)
        constraints.append(leftConstraint)
        constraints.append(bottomConstraint)
        constraints.append(rightConstraint)
        
        return constraints
    }
    
    // MARK: - Sizing Helper Methods
    
    /**
    Helper method which iterates through subviews assigning the preferredMaxLayoutWidth of any lables to be the size of the frame. This allows correct size calculations using AutoLayout.
    */
    func assignPreferredMaxLayoutWidths() {
        if let label = self as? UILabel {
            if label.preferredMaxLayoutWidth != label.frame.size.width {
                label.preferredMaxLayoutWidth = label.frame.size.width
                label.setNeedsLayout()
            }
        }
        
        for sv in subviews {
            sv.assignPreferredMaxLayoutWidths()
        }
    }
    
    // TODO: UnitTestable
    func compressedFittingSizeConstrainedTo(constrainedSize:CGSize) -> CGSize {
        
        let constrainedWidth = constrainedSize.width != UIViewNoIntrinsicMetric
        let constraintedHeight = constrainedSize.height != UIViewNoIntrinsicMetric
        
        if constrainedWidth && constraintedHeight {
            return constrainedSize;
        }
        
        if !(constrainedWidth || constraintedHeight) {
            return self.intrinsicContentSize()
        }
        
        let fittingAttribute:NSLayoutAttribute = constrainedWidth ? .Width : .Height
        let fittingValue = constrainedWidth ? constrainedSize.width : constrainedSize.height
        
        let fittingConstraint = NSLayoutConstraint(item: self,
            attribute: fittingAttribute,
            relatedBy: .LessThanOrEqual,
            toItem: nil,
            attribute: .NotAnAttribute,
            multiplier: 0.0,
            constant: fittingValue)
        
        let originalFrame = self.frame
        var sizingFrame = originalFrame
        
        if constrainedWidth {
            sizingFrame.size.width = fittingValue
        } else {
            sizingFrame.size.height = fittingValue
        }
        
        self.frame = sizingFrame
        addConstraint(fittingConstraint)
        
        // ensure all frames are correct by the new constraints
        layoutIfNeeded()
        
        // set any label widths to be their current frame so the fitting size adds any line breaks correctly.
        assignPreferredMaxLayoutWidths()
        
        // re-layout now labels will be sized correctly with the correct line breaks.
        layoutIfNeeded()
        
        // get the size according to AutoLayout
        let fittingSize = systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
        
        // return the cell to the original state
        self.frame = originalFrame
        removeConstraint(fittingConstraint)
        
        return fittingSize
    }
}

// MARK: - Debug Methods

extension UIView {
    
    func printRecursiveConstraints(atIndent: Int = 1) {
        printConstraints(atIndent)
        
        for subview in subviews {
            subview.printRecursiveConstraints(atIndent + 1)
        }
    }
    
    func printConstraints(atIndent: Int = 1) {
        print("\(self)\n")
        
        for constr in self.constraints {
            
            var indentStr = ""
            for _ in 0..<atIndent {
                indentStr += "-"
            }
            
            print("\(indentStr)\(constr)")
        }
        
        print("")
    }
}
