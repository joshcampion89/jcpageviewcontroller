//
//  UIViewController+JCExtensions.swift
//  Get Games
//
//  Created by Josh Campion on 25/01/2015.
//  Copyright (c) 2015 Josh Campion. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /**
    
    Helper method to allow editing of the read-only length property on the default UIViewController top and bottom `layoutGuides`. This would typically be used by custom container view controllers to set the `topLayoutGuide.length` and `bottomLayoutGuide.length` of child view controllers.
    
    - parameter insets: The values to set the top and bottom layout guides to.
    - parameter toViewController: The `UIViewController` whose view is the receiver.
    
    */
    func setLayoutConstraintLengths(insets: UIEdgeInsets) {
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        // As the layoutGuide properties of UIViewController are read-only, iterate through the constraints to find those corresponding to the layout guides.
        for constr in view.constraints {
            
            // println("assessing \(constr)")
            if topLayoutGuide === constr.firstItem && constr.secondItem == nil && constr.constant != insets.top {
                // println("Setting topLayoutGuide Length \(insets.top)")
                constr.constant = insets.top
            }
            
            if bottomLayoutGuide === constr.firstItem && constr.secondItem == nil && constr.constant != insets.bottom {
                // println("Setting bottomLayoutGuide Length \(insets.bottom)")
                constr.constant = insets.bottom
            }
        }
    }
}
