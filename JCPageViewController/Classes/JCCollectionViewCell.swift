//
//  JCCollectionViewCell.swift
//  JCPageViewController
//
//  Created by Josh Campion on 10/01/2015.
//  Copyright (c) 2015 Josh Campion. All rights reserved.
//

import UIKit

/**

`JCCollectionViewCell` is intended to be a base class for all `UICollectionViewCell` subclasses, providing three basic properties: two `UILabel`s and a `UIImageView`. This should allow most basic `UICollectionViewCell`s to be created without the need for a custom subclass for each situation.

*/
public class JCCollectionViewCell: UICollectionViewCell {
    
    /// Default variable to allow most basic cells to be created without needing a dedicated cell class. Typically this would be used as the title label.
    @IBOutlet public var textLabel:UILabel?

    /// Default variable to allow most basic cells to be created without needing a dedicated cell class. Typically this would be used as a sub-title label.
    @IBOutlet public var detailLabel:UILabel?
    
    /// Default variable to allow most basic cells to be created without needing a dedicated cell class. Typically this would be used as a thumbnail view.
    @IBOutlet public var imageView:UIImageView?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        customInit()
    }
    
    // TODO: Ensure the cell can be created without being initialised by an NSCoder
    /*
    override init() {
        super.init(coder: nil)
        
        if print_methods { println("\(self.debugDescription).\(__FUNCTION__)") }
        
        customInit()
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        customInit()
    }
    
    /// Called from each of the initialisers to easily set defaults.
    public func customInit() {
        
    }
}
