//
//  JCScrollingPageViewController.swift
//  JCPageViewController
//
//  Created by Josh Campion on 25/01/2015.
//  Copyright (c) 2015 Josh Campion. All rights reserved.
//

import UIKit

@objc public protocol JCScrollingPageChildViewController {
    /// The `JCScrollingPageViewController` uses this method to observe `contentOffset` changes in child page UIScrollViews allowing the user to drag on the child scroll view and that have an effect on the JCScrollingPageViewController's `scrollContainer`, hiding or showing the `headerView`.
    func scrollContainerForScrollingPageViewController(scrollingPageViewController: JCScrollingPageViewController) -> UIScrollView?
}

public class JCScrollingPageViewController: JCPageViewController {

    /**
    The height of the `headerView` which remains visible when the user scrolls.
    
    It is assumed that the subclass which provides the headerView will also constrain its height to a maximum size. It is the subclasses responsibility to update this value if the `headerView` changes appearance.
    */
    public var minimumHeaderHeight:CGFloat {
        didSet {
            if willAppear || didAppear {
                updatePageContainerHeight()
                view.setNeedsLayout()
            }
        }
    }

    // changing the pageControl height reduces the space left of the pageContainer so update its height.
    override public var pageControlHeight:CGFloat {
        didSet {    
            updatePageContainerHeight()
        }
    }
    
    /**

    The `pageContainer pageContainer's` height is constrained so that it fills the remainder of self.view when the headerView is at its minimum height.
    
    This allows pages which contain scrollView's themselves to not have all of their content in the view hierarchy at once. Consequently, child view controllers who contain scrollViews should conform to `JCScrollingPageChildViewController` to allow the JCScrollingPageViewController to manage auto-hiding of the header view. 
    
    An alternative method which didn't require the management of scroll view interactions would be to constrain the height of the pageContainer to be the height of its content and float the headerView and `pageControl`. This has the following disadvantages:

    - Child view controllers containing UITableViews and UICollectionViews reuse views for efficiency. If its view is the height of its content, those views cannot be reused and any size calculataions must be done when the view is added to this view hierarchy.
    - It is not clear how to mantain children's content offsets when navigating away from and back to a child if their content sizes are different.
    - It is not clear how the scrollContainer's content size should change when adding or removing pages whose content size differs.
    - This creates a more complex view hierarchy which is managed on scroll of a 'master' scrollview.
    
    Fixing the size of the child view controller's views and managing the scroll view interactions removes these problems.
    */
    @IBOutlet public var pageContainerHeightConstraint:NSLayoutConstraint?
    
    /// The scrollView which houses the view hierarchy. This enables the headerView to be scrolled off the top of the screen. Its content size is constrained by the size of the `headerView`, the size of the `pageControl `and the remaining portion of self.view when the header is at its minimum size (as this is the size the `pageContainer` is set too).
    @IBOutlet public var scrollContainer:UIScrollView?
    
    /// If the `currentViewController` contains a UIScrollView, the scrolling of which is supposed to interact with the header, and it conforms to the `JCScrollingPageChildViewController` protocol, then the UIScrollView's contentOffset is observed to manipulate the header on scroll. A reference is stored so the observer can be removed when the currentViewController changes or the view is destroyed.
    public var currentChildScrollView:UIScrollView?
    
    /// Flag to determine whether the a change in the ScrollView returned from the JCScrollingPageChildViewController scrollContainerForScrollingPageViewController method is scrolling because of user interaction of because it was adjusted programmatically. This is used when observing a change in the child scrollView's contentOffset to allowing scrolling the child to push up / pull down the `scrollContainer`.
    private (set) var isScrollingParent:Bool = false
    
    
    /// Subclasses should create their headerViews before super.viewDidLoad() or add them to `scrollContainer scrollContainer's` view hierarchy after super.viewDidLoad().
    @IBOutlet var headerView:UIView?
    
    // MARK: - Initialisers
    
    required public init?(coder aDecoder: NSCoder) {
        
        minimumHeaderHeight = 0.0
        
        super.init(coder: aDecoder)
    }
    
     deinit {
        if let childScroll = currentChildScrollView {
            childScroll.removeObserver(self, forKeyPath: "contentOffset")
        }
    }
    
    // MARK: - View lifecycle
    
    // Creates the necessary extra views needed, ensuring the superclass doesn't create its own view hierarchy. Then adds the superclass' controls to the scrollContainer.
    override public func viewDidLoad() {
        
        usesDefaultLayout = false
        
        super.viewDidLoad()
        
        if scrollContainer == nil {
            // create the default container
            let containerFrame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)
            
            let scrollView = UIScrollView(frame: containerFrame)
            scrollView.translatesAutoresizingMaskIntoConstraints = false
            scrollView.backgroundColor = UIColor.orangeColor()
            view.addSubview(scrollView)
            
            scrollContainer = scrollView
        }
        
        if let scrollView = scrollContainer {
            // set the necessary values for the scrollContainer
            scrollView.showsHorizontalScrollIndicator = false
            scrollView.showsVerticalScrollIndicator = false
            scrollView.pagingEnabled = false
            scrollView.bounces = false
            
            // create the view hierarchy
            if let headerView = self.headerView {
                scrollView.addSubview(headerView)
            }
            
            if let pageControl = self.pageControl {
                scrollView.addSubview(pageControl)
            }
            
            if let selectionIndicator = self.selectionIndicator {
                scrollView.addSubview(selectionIndicator)
            }
            
            if let pageContainer = self.pageContainer {
                scrollView.addSubview(pageContainer)
            }
        }
        
        pageContainer?.backgroundColor = UIColor.purpleColor()
        
        self.delegate = self
    }
    
    override public func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    // MARK: - Layout Methods
    
    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        let childLayoutGuides = UIEdgeInsetsMake(0, 0, bottomLayoutGuide.length, 0)
        adjustChildLayoutGuides(childLayoutGuides)
    }
    
    // As `usesDefaultLayout` is false for this class, override the standard `resetChildLayoutGuides`.
    override public func resetChildLayoutGuides() {
        let childLayoutGuides = UIEdgeInsetsMake(0, 0, bottomLayoutGuide.length, 0)
        adjustChildLayoutGuides(childLayoutGuides)
    }
    
    // MARK: - Constraint Methods
    
    // update to the default layout
    override public func updateViewConstraints() {
        
        super.updateViewConstraints()
        
        // ensure all views are created to use this default layout
        if (scrollContainer == nil) {
            print("<\(self.debugDescription): \(__FUNCTION__)>  Could not construct the default layout as scrollContainer is nil. If there is no page container use of this class is discouraged as the current implementation is probably over engineered for the purpose.")
        }
        
        if (pageContainer == nil) {
            print("<\(self.debugDescription): \(__FUNCTION__)>  Could not construct the default layout as pageContainer is nil. If there is no page container use of this class is discouraged as the current implementation is probably over engineered for the purpose.")
        }
        
        if (headerView == nil) {
            print("<\(self.debugDescription): \(__FUNCTION__)>  Could not construct the default layout as headerView is nil. If these is no headerView, use of this class is discouraged. JCPageViewController should likely be used instead.")
        }
        
        if (pageContainer == nil || headerView == nil || scrollContainer == nil) {
            return
        }
        
        if let scrollContainer = self.scrollContainer {
            if let pageContainer = self.pageContainer {
                if let headerView = self.headerView {
                    
                    // set the scroll container to be flush to the view edges
                    view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[scroll]|",
                        options: .DirectionLeadingToTrailing,
                        metrics: nil,
                        views: ["scroll":scrollContainer]))
                    
                    view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[scroll]|",
                        options: .DirectionLeadingToTrailing,
                        metrics: nil,
                        views: ["scroll":scrollContainer]))
                    
                    // fix the width of the scrollviews' content view by fixing the width of the header view
                    if let headerView = self.headerView {
                        view.addConstraint(NSLayoutConstraint(item: headerView,
                            attribute: .Width,
                            relatedBy: .Equal,
                            toItem: view,
                            attribute: .Width,
                            multiplier: 1.0,
                            constant: 0.0))
                    }
                    
                    // fix the all subviews to be flush to the scrollView's content view
                    if let scrollContainer = self.scrollContainer {
                        let views = [self.headerView, self.pageControl, self.pageContainer]
                        for v in views {
                            if let view = v {
                                scrollContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[view]|",
                                    options: .DirectionLeadingToTrailing,
                                    metrics: nil,
                                    views: ["view":view]))
                            }
                        }
                    }
                    
                    // align the subviews vertically
                    if let pageControl = self.pageControl {
                        scrollContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[headerView][pageControl][pageContainer]|",
                            options: .DirectionLeadingToTrailing,
                            metrics: nil,
                            views: ["headerView":headerView, "pageControl":pageControl, "pageContainer": pageContainer]))
                    } else {
                        scrollContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[headerView][pageContainer]|",
                            options: .DirectionLeadingToTrailing,
                            metrics: nil,
                            views: ["headerView":headerView, "pageContainer": pageContainer]))
                    }
                    
                    updatePageContainerHeight()
                }
            }
        }
    }
    
    /// The subview's heights are constrained based on properties of the header view. The pageContainer is constrained by the height of the view allowing the page control and the header to be visible at their minimum heights. It is assumed the headerView will define its own height by its intrinsic content size.
    public func updatePageContainerHeight() {
        
        if let pageContainer = self.pageContainer {
            let pageHeightDelta = pageControlHeight + minimumHeaderHeight + topLayoutGuide.length
            
            if pageContainerHeightConstraint == nil {
                let heightConstr = NSLayoutConstraint(item: pageContainer,
                attribute: .Height,
                relatedBy: .Equal,
                    toItem: view,
                    attribute: .Height,
                    multiplier: 1.0,
                    constant: -pageHeightDelta)
                view.addConstraint(heightConstr)
                pageContainerHeightConstraint = heightConstr
            } else {
                pageContainerHeightConstraint?.constant = -pageHeightDelta
            }
        }
    }
}

// MARK: - PageViewController Delegate

extension JCScrollingPageViewController : JCPageViewControllerDelegate {
    
    public func pageViewController(pageViewController: JCPageViewController, didChangeCurrentPageTo page: UIViewController, atIndex: Int) {
        
        if let childScroll = currentChildScrollView {
            childScroll.removeObserver(self, forKeyPath: "contentOffset")
        }
        
        if let childPage = page as? JCScrollingPageChildViewController {
            currentChildScrollView = childPage.scrollContainerForScrollingPageViewController(self)
        } else {
            currentChildScrollView = nil
        }
        
        if let childScroll = currentChildScrollView {
            childScroll.addObserver(self, forKeyPath: "contentOffset", options: [.Old, .New], context: nil)
        }
    }
    
    override public func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        
        if !(object === currentChildScrollView && keyPath == "contentOffset") {
            super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
        } else {
            if isScrollingParent {
                //                println("Observed from parent manipulation.")
                return
            }
            
            var childScrollDelta: CGPoint = CGPointZero;
            
            guard let oldOffset = (change?[NSKeyValueChangeOldKey] as? NSValue)?.CGPointValue(),
                let newOffset = (change?[NSKeyValueChangeNewKey] as? NSValue)?.CGPointValue(),
            let scrollContainer = self.scrollContainer else { return }
            
            let deltaX = newOffset.x - oldOffset.x
            let deltaY = newOffset.y - oldOffset.y
            childScrollDelta = CGPoint(x: deltaX, y: deltaY)
            
            // println("Child Scrolled \(NSStringFromCGPoint(childScrollDelta)): \(NSStringFromCGPoint(oldOffset)) -> \(NSStringFromCGPoint(newOffset))")
            
            let maxScrollContainerOffsetY = scrollContainer.contentSize.height + scrollContainer.contentInset.bottom - scrollContainer.frame.size.height
            let minScrollContainerOffsetY = 0 - scrollContainer.contentInset.top
            
            let minChildOffsetY = 0 - currentChildScrollView!.contentInset.top
            
            // calculate the offset if the scroll was in the parent's view
            var proposedParentOffset = scrollContainer.contentOffset
            proposedParentOffset.y += childScrollDelta.y
            // clip to ensure this motion doesn't pull the scroll container too far
            proposedParentOffset.y = max(min(proposedParentOffset.y, maxScrollContainerOffsetY), minScrollContainerOffsetY)
            
            
            if proposedParentOffset.y != scrollContainer.contentOffset.y {
                
                // if the header is at least partially visible and the user scrolls up, the header should scroll up to its minimum height
                let shouldScrollParentUp = childScrollDelta.y > 0 && newOffset.y > minChildOffsetY
                
                // if the header is at its minimum height and the user scrolls down when the child scroll view is at its top, scroll the header down to its maximum height (don't bounce the header), then bounce the child
                let shouldScrollParentDown = childScrollDelta.y < 0 && newOffset.y < minChildOffsetY
                
                
                if shouldScrollParentUp || shouldScrollParentDown {
                    
                    isScrollingParent = true
                    //                                    println("Scrolling Parent: \(childScrollDelta.y)")
                    
                    scrollContainer.contentOffset = proposedParentOffset
                    currentChildScrollView?.contentOffset = oldOffset
                    isScrollingParent = false
                }
            }
            
        }
    }
}
