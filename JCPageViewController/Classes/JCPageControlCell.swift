//
//  JCPageControlCell.swift
//  JCPageViewController
//
//  Created by Josh Campion on 10/01/2015.
//  Copyright (c) 2015 Josh Campion. All rights reserved.
//

import UIKit
import TZStackView

public let JCPageControlCellIdentifier = "JCPageControlCell"

/**

This simple `UICollectionViewCell` is intended for use in the `pageControl` property of a JCPageViewController. It contains JCCollectionView's `textLabel`, `detailLabel` and `imageView` in a stack which is inset from the edges of this cell's `contentView` by the `insets` property when using the default layout. Typically this cell would not be created from a UIStoryboard, but programmatically from the `JCPageViewController`, as it registered against `JCPageControlCellIdentifier`.

 The default layout can be adjusted by setting the class variables
 - `stackInsets`
 - `stackSpacing`
 - `textSpacing`
 
 These are class variables to ensure consistency between cells in the page control. The default `UICollectionViewLayout` for use with this cell is `JCPageControlCollectionViewFlowLayout` which uses the value of these properties to correctly calculate the fitting size for a given `JCPageControlCell`. Whilst discouraged, a cell specific value can be set as each of these has an instance variable equivalent.
 
*/
public class JCPageControlCell: UICollectionViewCell {

    // MARK: - Properties
    
    // MARK: Class Properties
    
    /// The buffer around the `stack` to the `contentView` of the cell. Defaults to (12, 16, 12, 16).
    public static var stackInsets:UIEdgeInsets = UIEdgeInsetsMake(12, 16, 12, 16)

    /// The spacing for the stack view between the image and `textStack`. Defaults to 8.0.
    public static var stackSpacing:CGFloat = 8
    
    /// The spacing between the `titleLabel` and `subtitleLabel`. Defaults to 4.0.
    public static var textSpacing:CGFloat = 4
    
    // MARK: Instance Properties
    
    /// The cell specific value of the class variable of the same name. This defaults to the class variable, but can be overriden for a specific cell.
    public var stackInsets:UIEdgeInsets = JCPageControlCell.stackInsets {
        didSet {
            setNeedsUpdateConstraints()
        }
    }
    
    /// The cell specific value of the class variable of the same name. This defaults to the class variable, but can be overriden for a specific cell.
    public var stackSpacing:CGFloat = JCPageControlCell.stackSpacing {
        didSet {
            stack.spacing = stackSpacing
        }
    }
    
    /// The cell specific value of the class variable of the same name. This defaults to the class variable, but can be overriden for a specific cell.
    public var textSpacing:CGFloat = JCPageControlCell.textSpacing {
        didSet {
            textStack.spacing = textSpacing
        }
    }
    
    // MARK: - SubViews
    
    /// Stack containing the `imageView` and the `textStack`. Axis is `.Horizontal`, if this is changed, subclasses should override `fittingSizeForWidth(...)` to return the correct value. Spacing can be controlled using the `stackSpacing` properties.
    public var stack:TZStackView!
    
    /// Stack containing the `titleLabel` and `subtitleLabel`. Axis is `.Vertical`, if this is changed, subclasses should override `fittingSizeForWidth(...)` to return the correct value. Spacing can be controlled using the `textSpacing` properties.
    public var textStack:TZStackView!
    
    /// The four constraints aligning `stack` to the cell's `contentView`. The constants for these constraints can be controlled using `stackInsets`.
    private(set) var stackConstraints:[NSLayoutConstraint]!
    
    /// A `UILabel` to display the title of this page. This is created and configured in `configureViews()` and added to `textStack` in `createViewHierarchy()`. Subclasses can override these methods to customise this label. It is set to Center text alignment and `UIFontTextStyleHeadline`.
    public var titleLabel = UILabel()

    /// A `UILabel` to display the subtitle of this page. This is created and configured in `configureViews()` and added to `textStack` in `createViewHierarchy()`. Subclasses can override these methods to customise this label. It is set to Center text alignment and `UIFontTextStyleCaption1`.
    public var subtitleLabel = UILabel()
    
    /// A `UIImageView` to display an icon for this page. This is created and configured in `configureViews()` and added to `stack` in `createViewHierarchy()`. Subclasses can override these methods to customise this icon.
    public var imageView = UIImageView()
    
    // MARK: - Initialisers
    
    /**

     Used in the initialiser to create the constraints to align `stack` to the `contentView`.
     
     - returns: An array of `NSLayoutConstraint`s aligning the stack to the top, leading, bottom and trailing edges of the cell's `contentView`, by the amounts given by the `stackInsets` property.
    */
    func createStackConstraints() -> [NSLayoutConstraint] {
        
        let top = NSLayoutConstraint(item: stack,
            attribute: .Top,
            relatedBy: .Equal,
            toItem: contentView,
            attribute: .Top,
            multiplier: 1.0,
            constant: stackInsets.top)
        
        let left = NSLayoutConstraint(item: stack,
            attribute: .Leading,
            relatedBy: .Equal,
            toItem: contentView,
            attribute: .Leading,
            multiplier: 1.0,
            constant: stackInsets.left)
        
        let bottom = NSLayoutConstraint(item: stack,
            attribute: .Bottom,
            relatedBy: .Equal,
            toItem: contentView,
            attribute: .Bottom,
            multiplier: 1.0,
            constant: -stackInsets.bottom)
        
        let right = NSLayoutConstraint(item: stack,
            attribute: .Trailing,
            relatedBy: .Equal,
            toItem: contentView,
            attribute: .Trailing,
            multiplier: 1.0,
            constant: -stackInsets.right)
        
        return [top, left, bottom, right]
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        createViewHierarchy()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    /// Sets the properties for the `titleLabel`, `subtitleLabel` and `imageView`. Subclasses should override this method to customize those variable or initialise them with other `UILabel` or `UIImageView` subclasses.
    public func configureViews() {
        
        titleLabel.textAlignment = .Center
        titleLabel.font = UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline)
        
        subtitleLabel.textAlignment = .Center
        subtitleLabel.font = UIFont.preferredFontForTextStyle(UIFontTextStyleCaption1)
        subtitleLabel.hidden = true
        
        imageView.hidden = true
    }
    
    
    /// Called from `init(frame:)` to create the default layout. This initialiser is called when a class has been registered to a `UICollectionView` using `registerClass(_:forCellWithReuseIdentifier:)`. If a custom layout from a Storyboard or xib is being used, subclasses should override `fittingSizeForWidth(...)` to return the correct value for the custom layout.
    public func createViewHierarchy() {
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        configureViews()
        
        textStack = TZStackView(arrangedSubviews: [titleLabel, subtitleLabel])
        textStack.spacing = textSpacing
        textStack.axis = .Vertical
        
        stack = TZStackView(arrangedSubviews: [imageView, textStack])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .Horizontal
        stack.spacing = stackSpacing
        
        contentView.addSubview(stack)
        
        stackConstraints = createStackConstraints()
        
        contentView.addConstraints(stackConstraints)
        
        JCPageControlCell.addObserver(self, forKeyPath: "stackInsets", options: [.Old, .New], context: nil)
        JCPageControlCell.addObserver(self, forKeyPath: "textSpacing", options: [.Old, .New], context: nil)
        JCPageControlCell.addObserver(self, forKeyPath: "stackSpacing", options: [.Old, .New], context: nil)
        
        /*
        // colouring for debug
        textLabel?.textColor = UIColor.whiteColor()
        textLabel?.backgroundColor = UIColor.greenColor()
        
        self.contentView.backgroundColor = UIColor.clearColor()
        
        let selView = UIView(frame: bounds)
        selView.backgroundColor = UIColor.yellowColor()
        
        let bkView = UIView(frame: bounds)
        bkView.backgroundColor = UIColor.blackColor()
        
        self.backgroundView = bkView
        self.selectedBackgroundView = selView
        */
    }
    
    deinit {
        JCPageControlCell.removeObserver(self, forKeyPath: "stackInsets")
    }
    
    
    public override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if keyPath == "stackInsets" {
            
            guard let oldClassInsets = (change?[NSKeyValueChangeOldKey] as? NSValue)?.UIEdgeInsetsValue(),
                let newClassInsets = (change?[NSKeyValueChangeNewKey] as? NSValue)?.UIEdgeInsetsValue()
                // only update this cells insets if they haven't been manually changed, i.e. they match the class value
                where UIEdgeInsetsEqualToEdgeInsets(oldClassInsets, stackInsets) &&
                    !UIEdgeInsetsEqualToEdgeInsets(oldClassInsets, newClassInsets)
                else { return }
            
            stackInsets = newClassInsets
            
        } else if keyPath == "stackSpacing" || keyPath == "textSpacing" {
            
            guard let oldClassSpacing = change?[NSKeyValueChangeOldKey] as? CGFloat,
            let newClassSpacing = change?[NSKeyValueChangeNewKey] as? CGFloat
                where oldClassSpacing != newClassSpacing else { return }
            
            // only update this cells spacings if they haven't been manually changed, i.e. they match the class value
            if keyPath == "stackSpacing" && oldClassSpacing == stackSpacing {
                stackSpacing = newClassSpacing
            } else if keyPath == "textSpacing" && oldClassSpacing == textSpacing {
                textSpacing = newClassSpacing
            }
        }
        
    }
    
    // MARK: - Layout methods
    
    public static func fittingSizeForWidth(width:CGFloat, title:String?, titleFont:UIFont?, subtitle:String?, subtitleFont:UIFont?, image:UIImage?, stackInsets:UIEdgeInsets = JCPageControlCell.stackInsets, stackSpacing:CGFloat = JCPageControlCell.stackSpacing, textSpacing:CGFloat = JCPageControlCell.textSpacing) -> CGSize {
        
        let imageSize:CGSize? = image?.size
        
        // caculate the size available for the labels
        var textWidth = width - (stackInsets.left + stackInsets.right)
        
        if let size = imageSize {
            textWidth -= size.width + stackSpacing
        }
        
        let textConstrainSize = CGSizeMake(textWidth, CGFloat.max)
        
        // calculate label sizes
        let titleSizeO:CGSize?
        if let text = title,
            let font = titleFont {
                
                let textRect = (text as NSString).boundingRectWithSize(textConstrainSize,
                    options: .UsesLineFragmentOrigin,
                    attributes: [NSFontAttributeName: font],
                    context: nil)
                
                titleSizeO = CGRectIntegral(textRect).size
        } else {
            titleSizeO = nil
        }
        
        let subtitleSizeO:CGSize?
        if let text = subtitle,
            let font = subtitleFont {
                
                let textRect = (text as NSString).boundingRectWithSize(textConstrainSize,
                    options: .UsesLineFragmentOrigin,
                    attributes: [NSFontAttributeName: font],
                    context: nil)
                
                subtitleSizeO = CGRectIntegral(textRect).size
        } else {
            subtitleSizeO = nil
        }
        
        // calculate the size of the text stack
        let textSizeO:CGSize?
        switch (titleSizeO, subtitleSizeO) {
        case let (.Some(titleSize), .Some(subtitleSize)):
            
            let width = max(titleSize.width, subtitleSize.width)
            let height = titleSize.height + textSpacing + subtitleSize.height
            
            textSizeO = CGSizeMake(width, height)
        case let (.Some(titleSize), .None):
            textSizeO = titleSize
        case let (.None, .Some(subtitleSize)):
            textSizeO = subtitleSize
        case (.None, .None):
            textSizeO = nil
        }
        
        // calculate the stack
        let stackSize:CGSize
        switch (imageSize, textSizeO) {
        case let (.Some(img), .Some(text)):
            
            let width = img.width + stackSpacing + text.width
            let height = max(img.height, text.height)
            
            stackSize = CGSizeMake(width, height)
            
        case let (.Some(img), .None):
            stackSize = img
        case let (.None, .Some(text)):
            stackSize = text
        case (.None, .None):
            stackSize = CGSizeZero
        }
        
        // calculate total size including insets
        let fittingSize = CGSizeMake(stackSize.width + stackInsets.left + stackInsets.right, stackSize.height + stackInsets.top + stackInsets.bottom)
        return fittingSize
    }
    
    // Configures the `stackConstraint`s constant to match the `stackInsets` property.
    override public func updateConstraints() {
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        for (n, inset) in [stackInsets.top, stackInsets.left, -stackInsets.bottom, -stackInsets.right].enumerate() {
            let thisConstraint = stackConstraints[n]
            
            if thisConstraint.constant != inset {
                thisConstraint.constant = inset
                
            }
        }
        
        super.updateConstraints()
    }

}
