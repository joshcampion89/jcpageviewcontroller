//
//  UICollectionViewDelegateJCPageControlFlowLayout.swift
//  JCPageController
//
//  Created by Josh Campion on 30/12/2015.
//
//

import UIKit

/// Delegate for `JCPageControlCollectionViewFlowLayout` that provides the layout with the necessary information to calculate the sizes for each cell. Default implementations are provided for each method by a protocol extension.
public protocol UICollectionViewDelegatePageControlLayout: UICollectionViewDelegateFlowLayout {
    
    /**
     
     Asks the delegate for the title for the `JCPageControlCell` for this item, used for cell size calculations.
     
     - parameter collectionView: The collection view object displaying the `JCPageControlCollectionViewFlowLayout`.
     - parameter collectionViewLayout: The `JCPageControlCollectionViewFlowLayout` requesting the information.
     - parameter indexPath: The index path of the item.
     
     - returns: The title, or `nil` if none is to be shown. Default implementation returns nil.
     */
    func collectionView(collectionView:UICollectionView, layout:UICollectionViewLayout, titleForIndexPath indexPath:NSIndexPath) -> String?
    
    /**
     
     Asks the delegate for the title font for the `JCPageControlCell` for this item, used for cell size calculations.
     
     - parameter collectionView: The collection view object displaying the `JCPageControlCollectionViewFlowLayout`.
     - parameter collectionViewLayout: The `JCPageControlCollectionViewFlowLayout` requesting the information.
     - parameter indexPath: The index path of the item.
     
     - returns: The title font, or `nil` if none is to be shown. Default implementation returns `UIFontTextStyleHeadline` as per the defualt for the `titleLabel` of a `JCPageControlCell`.
     
     */
    func collectionView(collectionView:UICollectionView, layout:UICollectionViewLayout, titleFontForIndexPath indexPath:NSIndexPath) -> UIFont?
    
    /**
     
     Asks the delegate for the subtitle for the `JCPageControlCell` for this item, used for cell size calculations.
     
     - parameter collectionView: The collection view object displaying the `JCPageControlCollectionViewFlowLayout`.
     - parameter collectionViewLayout: The `JCPageControlCollectionViewFlowLayout` requesting the information.
     - parameter indexPath: The index path of the item.
     
     - returns: The subtitle, or `nil` if none is to be shown. Default implementation returns nil.
     */
    func collectionView(collectionView:UICollectionView, layout:UICollectionViewLayout, subtitleForIndexPath indexPath:NSIndexPath) -> String?
    
    /**
     
     Asks the delegate for the subtitle font for the `JCPageControlCell` for this item, used for cell size calculations.
     
     - parameter collectionView: The collection view object displaying the `JCPageControlCollectionViewFlowLayout`.
     - parameter collectionViewLayout: The `JCPageControlCollectionViewFlowLayout` requesting the information.
     - parameter indexPath: The index path of the item.
     
     - returns: The subtitle font, or `nil` if none is to be shown. Default implementation returns `UIFontTextStyleCaption1` as per the defualt for the `subtitleLabel` of a `JCPageControlCell`.
     
     */
    func collectionView(collectionView:UICollectionView, layout:UICollectionViewLayout, subtitleFontForIndexPath indexPath:NSIndexPath) -> UIFont?
    
    /**
     
     Asks the delegate for the image for the `JCPageControlCell` for this item, used for cell size calculations.
     
     - parameter collectionView: The collection view object displaying the `JCPageControlCollectionViewFlowLayout`.
     - parameter collectionViewLayout: The `JCPageControlCollectionViewFlowLayout` requesting the information.
     - parameter indexPath: The index path of the item.
     
     - returns: The image, or `nil` if none is to be shown. Default implementation returns nil.
     */
    func collectionView(collectionView:UICollectionView, layout:UICollectionViewLayout, imageForIndexPath indexPath:NSIndexPath) -> UIImage?
    
    /**
     
     Asks the delegate for the `stackInsets` for the `JCPageControlCell` for this item, used for cell size calculations.
     
     - parameter collectionView: The collection view object displaying the `JCPageControlCollectionViewFlowLayout`.
     - parameter collectionViewLayout: The `JCPageControlCollectionViewFlowLayout` requesting the information.
     - parameter indexPath: The index path of the item.
     
     - returns: Default implementation returns the current class variable, `JCPageControl.stackInsets`.
     */
    func collectionView(collectionView:UICollectionView, layout:UICollectionViewLayout, stackInsetsForIndexPath indexPath:NSIndexPath) -> UIEdgeInsets
    
    /**
     
     Asks the delegate for the `textSpacing` for the `JCPageControlCell` for this item, used for cell size calculations.
     
     - parameter collectionView: The collection view object displaying the `JCPageControlCollectionViewFlowLayout`.
     - parameter collectionViewLayout: The `JCPageControlCollectionViewFlowLayout` requesting the information.
     - parameter indexPath: The index path of the item.
     
     - returns: Default implementation returns the current class variable, `JCPageControl.textSpacing`.
     */
    func collectionView(collectionView:UICollectionView, layout:UICollectionViewLayout, textSpacingForIndexPath indexPath:NSIndexPath) -> CGFloat
    
    /**
     
     Asks the delegate for the `stackSpacing` for the `JCPageControlCell` for this item, used for cell size calculations.
     
     - parameter collectionView: The collection view object displaying the `JCPageControlCollectionViewFlowLayout`.
     - parameter collectionViewLayout: The `JCPageControlCollectionViewFlowLayout` requesting the information.
     - parameter indexPath: The index path of the item.
     
     - returns: Default implementation returns the current class variable, `JCPageControl.stackSpacing`.
     */
    func collectionView(collectionView:UICollectionView, layout:UICollectionViewLayout, stackSpacingForIndexPath indexPath:NSIndexPath) -> CGFloat
}