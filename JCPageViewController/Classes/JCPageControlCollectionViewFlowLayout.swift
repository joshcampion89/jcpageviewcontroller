//
//  JCPageControlCollectionViewFlowLayout.swift
//  JCPageViewController
//
//  Created by Josh Campion on 19/12/2015.
//
//

import UIKit
import TZStackView

let DefaultTabSize = CGSizeMake(100, 44)

/// `NSNotification` name for a notification that is posted when the `maxHeight` property of a `JCPageControlCollectionViewFlowLayout` changes.
public let JCPageControlHeightChanged = "JCPageControlHeightChangedNotification"

/// Enum for how the cells in a `JCPageControlCollectionViewFlowLayout` are laid out when the cells' fitting sizes don't exceed the UICollectionView's frame.
public enum JCPageControlCellDistribution {
    /// Cells are sized to their fitting value and left aligned to the frame of the `UICollectionView`.
    case Left
    /// Cells are sized to their fitting value and collectively centred in the frame of the `UICollectionView`.
    case Center
    /// Cells are sized to fill the frame of the `UICollectionView`, with sizes scaled relatively to their fitting sizes. This is the default value.
    case FillProportionally
}

/**
 
 Default layout for a `JCPageViewController`. Each cell is sized to fit two strings and an image, as determined by a `UICollectionViewDelegatePageControlLayout`, which should be the delegate of this layout's `UICollectionView`.

 This uses the `UICollectionViewDelegatePageControlLayout` methods and `JCPageControlCell.fittingSizeForWidth(...)` to calculate a size for each cell in its layout that can be accessed in `cellSizes` for use in `collectionView(_:layout:sizeForItemAtIndexPath:)`. Sizes are calculated assuming that this layout is horizontally scrolling. 
 
 This layout can be customised using `cellAlignment`, `cellInset`, `UICollectionViewDelegatePageControlLayout` methods and the class properties on `JCPageControlCell`.
 
*/
public class JCPageControlCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    /// The alignment of the cells should they not fill or exceed `collectionView?.frame.size.width`. Defaults to `.FillProportionally`. Setting this to a new value invalidates the layout.
    public var cellAlignment:JCPageControlCellDistribution = .FillProportionally {
        didSet {
            if cellAlignment != oldValue {
                invalidateLayout()
            }
        }
    }
    
    /// The inset from the left and right edges of the collection view's frame that determines the maximum width of any cell
    public var cellInset:CGFloat = 16.0 {
        didSet {
            self.invalidateLayout()
        }
    }
    
    /// Dictionary to hold the actual frame sizes for each cell. This can differ from the actual sizes given in `cellSizes` depending on the fitting height of other cells and `cellAlignment`.
    public var cellSizes = [NSIndexPath: CGSize]()
    
    /// Dictionary to hold the calculated fitting sizes for each cell. This can differ from the actual sizes given in `cellSizes` depending on the fitting height of other cells and `cellAlignment`.
    public var cellFittingSizes = [NSIndexPath: CGSize]()
    
    /// The height of the tallest cell, as calculated in `prepareForLayout()`. If this value changes, an `NSNotification` is posted to the default center named `JCPageControlHeightChanged` with this layout as the `object`.
    public private (set) var maxHeight: CGFloat = 0 {
        didSet {
            if maxHeight != oldValue {
                NSNotificationCenter.defaultCenter().postNotificationName(JCPageControlHeightChanged, object: self)
            }
        }
    }
    
    override public func prepareLayout() {
        
        guard let collectionView = self.collectionView,
            let dataSource = collectionView.dataSource else { return }
        
        // clear the cached values from any previous calculations
        cellSizes.removeAll()
        cellFittingSizes.removeAll()
        maxHeight = 0
        
        let sectionCount = dataSource.numberOfSectionsInCollectionView?(collectionView) ?? 1
        
        for s in 0..<sectionCount {

            let itemCount = dataSource.collectionView(collectionView, numberOfItemsInSection: s)
            
            for i in 0..<itemCount {
                let path = NSIndexPath(forItem: i, inSection: s)
                let size = calculateFittingSizeForIndexPath(path)
                cellFittingSizes[path] = size
                if size.height > maxHeight {
                    maxHeight = size.height
                }
            }
        }
        
        let separation = minimumLineSpacing
        let totalFittingWidth = cellFittingSizes.values.reduce(0, combine: { $0 + $1.width + separation }) - separation
        
        let sizeSequence:[(NSIndexPath, CGSize)]
        let cellSpace = collectionView.frame.size.width
        if cellSpace > totalFittingWidth && cellAlignment == .FillProportionally {
            
            let multiplier = cellSpace / totalFittingWidth
            sizeSequence = cellFittingSizes.map({ ($0, CGSizeMake(floor($1.width * multiplier), maxHeight)) })
            
        } else {
            sizeSequence = cellFittingSizes.map({ ($0, CGSizeMake($1.width, maxHeight)) })
        }
        
        var insets = collectionView.contentInset
        if cellAlignment == .Center {
            insets.left = max(0.5 * (cellSpace - totalFittingWidth), 0)
            collectionView.contentInset = insets
        } else {
            insets.left = 0
            collectionView.contentInset = insets
        }
        
        for (k,v) in sizeSequence {
            cellSizes[k] = v
        }
        
        // So that the `UICollectionViewDelegateFlowLayout method `collectionView(_:layout:sizeForItemAtIndexPath:)` can be utilised the cell sizes are calculated before calling to super.
        super.prepareLayout()
    }
    
    /**
     
     Calls each of the `UICollectionViewDelegatePageControlLayout` methods to get the properties for this cell and calls through to `JCPageControlCell.fittingSizeForWidth(...)` to get the fitting size for the appropriate cell.
     
     - parameter indexPath: The path of the cell to calculate the size of.
     - returns: The fitting size for the appropriate cell.
    */
    public func calculateFittingSizeForIndexPath(indexPath:NSIndexPath) -> CGSize {
        
        guard let collectionView = self.collectionView,
            let delegate = collectionView.delegate as? UICollectionViewDelegatePageControlLayout else { return DefaultTabSize }
        
        let image = delegate.collectionView(collectionView, layout: self, imageForIndexPath: indexPath)
        
        let title = delegate.collectionView(collectionView, layout: self, titleForIndexPath: indexPath)
        let titleFont = delegate.collectionView(collectionView, layout: self, titleFontForIndexPath: indexPath)
        
        let subtitle = delegate.collectionView(collectionView, layout: self, subtitleForIndexPath: indexPath)
        let subtitleFont = delegate.collectionView(collectionView, layout: self, subtitleFontForIndexPath: indexPath)
        
        let stackInsets = delegate.collectionView(collectionView, layout: self, stackInsetsForIndexPath: indexPath)
        let stackSpacing = delegate.collectionView(collectionView, layout: self, stackSpacingForIndexPath: indexPath)
        let textSpacing = delegate.collectionView(collectionView, layout: self, textSpacingForIndexPath: indexPath)
        
        let maxWidth = collectionView.frame.size.width - 2.0 * cellInset
        
        let fittingSize = JCPageControlCell.fittingSizeForWidth(maxWidth,
            title: title,
            titleFont: titleFont,
            subtitle: subtitle,
            subtitleFont: subtitleFont,
            image: image,
            stackInsets: stackInsets,
            stackSpacing: stackSpacing,
            textSpacing: textSpacing)
        
        return fittingSize
    }
    
    /** 

     On a height bounds change the cell heights will need to be calcualted. On a width bounds change each cell has more fitting room. Also for layouts where the fitting widths exceed `collectionView?.frame.size.height` the actual size should be recalulated.
     
     - returns: true
    */
    override public func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
        // call invalidate layout so it knows the layout nedes to ask its delegate for the sizes again
        invalidateLayout()
        return true
    }
}
