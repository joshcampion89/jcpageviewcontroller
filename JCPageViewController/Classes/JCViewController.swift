//
//  JCViewController.swift
//  JCPageViewController
//
//  Created by Josh Campion on 12/01/2015.
//  Copyright (c) 2015 Josh Campion. All rights reserved.
//

import UIKit

/**

`JCViewController` is intended to be a base class for `UIViewController`s, providing `bool` flags on appearance.

*/
public class JCViewController: UIViewController {

    /// Set to `true` in `viewWillAppear(_)` and `false` in `viewDidDisappear(_)`, this is useful for performing conditional layout in `viewDidLayoutSubviews()`. Typical useage is with `didAppear` for pre-setting the `contentOffset` of a UIScrollView, which is reset to `CGPointZero` at some point between `viewWillAppear(_)` and `viewDidAppear(_)`.
    private(set) public var willAppear = false
    
    /// Set to `true` in `viewDidAppear(_)` and false in `viewDidDisappear(_)`, this is useful for performing conditional layout in `viewDidLayoutSubviews()`. Typical useage is with `willAppear` for pre-setting the `contentOffset` of a UIScrollView, which is reset to `CGPointZero` at some point between `viewWillAppear(_)` and `viewDidAppear(_)`.
    private(set) public var didAppear = false
    
    /// Set to true in `viewDidAppear(_)`, this can be useful for pre-setting a `contentOffset` but maintaining the user's scroll if another `UIViewController` is shown and then removed.
    private(set) public var firstAppeared = false
    
    /// Set to `true` in `viewWillDisappear(_)` and `false` in `viewDidAppear(_)`, this mirrors `willAppear`.
    private(set) public var willDisappear = false

    /// Set to `true` in `viewDidDisappear(_)` and `false` in `viewDidAppear(_)`, this mirrors `didAppear`.
    private(set) public var didDisappear = false
    
    // MARK: - View Lifecycle Methods
    
    override public func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        willAppear = true
    }
    
    override public func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        didAppear = true
        firstAppeared = true
        
        willDisappear = false
        didDisappear = false
    }
    
    override public func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        willDisappear = true
    }
    
    override public func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        didDisappear = true
        
        willAppear = false
        didAppear = false
    }
    
}
