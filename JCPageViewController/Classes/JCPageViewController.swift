//
//  JCPageViewController.swift
//  JCPageViewController
//
//  Created by Josh Campion on 02/01/2015.
//  Copyright (c) 2015 Josh Campion. All rights reserved.
//

import UIKit

let print_methods = false
let debug_paging = false
let debug_layout = false

let debug_colours = false

let kDefaultPageControlCellIdentifier = "PageControlCell"

/// Protocol defining ptional methods that allow a delegate to be informed of changes to the pages of a `JCPageViewController`.
@objc public protocol JCPageViewControllerDelegate {
    /**
     
     Called to `JCPageViewController JCPageViewController's ` `delegate` just before addChildViewController(:page) is called and the view added to the JCPageViewController's view hierarchy.
     
     - parameter pageViewController: The JCPageViewController adding the page
     - parameter page: The UIViewController being added to the JCPageViewController's view hierarchy.
     - parameter atIndex: The index of page in the JCPageViewController's `viewControllers` property.
     */
    optional func pageViewController(pageViewController:JCPageViewController, willAddPage page: UIViewController, atIndex:Int)
    
    /**
     
     Called to `JCPageViewController JCPageViewController's ` `delegate` right after page.didMoveToParentViewController(_:) is called when the view has been added to the JCPageViewController's view hierarchy.
     
     - parameter pageViewController: The JCPageViewController adding the page
     - parameter page: The UIViewController being added to the JCPageViewController's view hierarchy.
     - parameter atIndex: The index of page in the JCPageViewController's `viewControllers` property.
     */
    optional func pageViewController(pageViewController:JCPageViewController, didAddPage page: UIViewController, atIndex:Int)
    
    /**
     
     Called to `JCPageViewController JCPageViewController's ` `delegate` from updatePages() when the `currentViewController` property changes.
     
     - parameter pageViewController: The JCPageViewController managing the display of pages.
     - parameter page: The UIViewController now set to the currentViewController property.
     - parameter atIndex: The index of page in the JCPageViewController's `viewControllers` property.
     */
    
    optional func pageViewController(pageViewController:JCPageViewController, didChangeCurrentPageTo page: UIViewController, atIndex:Int)
    
    /**
     
     Called to `JCPageViewController JCPageViewController's ` `delegate` just before willMoveToParentViewController(nil) is called on page and the view removed from the JCPageViewController's view hierarchy.
     
     - parameter pageViewController: The JCPageViewController removing the page
     - parameter page: The UIViewController being removed from the JCPageViewController's view hierarchy.
     - parameter atIndex: The index of page in the JCPageViewController's `viewControllers` property.
     */
    optional func pageViewController(pageViewController:JCPageViewController, willRemovePage page: UIViewController, fromIndex:Int)
    
    /**
     
     Called to `JCPageViewController JCPageViewController's ` `delegate` right after removeFromParentViewController() is called on page and the view removed from the JCPageViewController's view hierarchy.
     
     - parameter pageViewController: The JCPageViewController removing the page
     - parameter page: The UIViewController being removed from the JCPageViewController's view hierarchy.
     - parameter atIndex: The index of page in the JCPageViewController's `viewControllers` property.
     */
    optional func pageViewController(pageViewController:JCPageViewController, didRemovePage page: UIViewController, fromIndex:Int)
}

/**
 
 Class to replace the standard `UIPageViewController`. It replicates the most common use case of a `UIPageViewController`; a single onscreen child controller scrolling horizontally. The default implementation mimics the 'Tabs' navigation element found in [Google Material Design](https://www.google.com/design/spec/components/tabs.html#). This shows a top bar with the title of each child view controller as a tappable cell, allowing direct navigation between pages.
 
 Its main benefit is the default layout and page control, however it also has a simpler API for anything other than large numbers of child view controllers and improved sending of view lifecycle methods. These methods are called consecutively in a `UIPageViewController`, which causes layout issues on iOS 8 where self sizing cells are calculated to be the wrong size. This is not a drop in replacement for `UIPageViewController`, there are API differences.
 
 The Page Container
 ------------------
 
 The `pageContainer UIScrollView` is the size required to hold all the child `UIViewController` views. The views are then added to the `pageContainer`'s content view as the `pageContainer` scrolls. The current, previous and next view controllers are all added to the hierarchy and the lifecycle methods are called as follows:
 
 - `viewWillAppear(_:)`: Called when the view is added to the hierarchy.
 - `viewDidAppear(_:)`: Called when the view becomes the `currentViewController`
 - `viewWillDisapper(_:)`: Called when the view is no longer the previous / current / next
 - `viewDidDisapper(_:)`: Called when the view has been removed from hierarchy. This is called very quickly after `viewWillDisapper(_:)`.
 - Each lifecycle method is forwarded to the previous / current / next view controllers when they are called on this viewController.
 
 Each view is loaded when it is added to the hierarchy in `updatePages()` if it is the view of the previous / current / next view controller properties. A `UIViewController`'s view is removed from the view hierarchy when the `UIViewController` is no longer the previous / next / current, it is never un-loaded. Typically this will not be a problem if there are not a large number of `UIViewController`s. If there are an indefinite number of pages, its is likely that the `UIPageViewController` with its dynamically provided child `UIViewController`s is more appropriate.
 
 The `pageContainer` property is exposed as an IBOutlet to allow more flexibility with layout. All child controllers are set to be the same size as `pageContainer.frame`. However if not set, a default layout will be implemented.
 
 The default layout is only implemented if the `pageContainer` property is not set in the Storyboard and the `showsPageControl` property is `true` (the default value).  The default `selectionIndicator` is only used if one is not set in the Storyboard and the `showsSelectionIndicator` property is true (the default value). The default cell is only used if `collectionView(_:cellForItemAtIndexPath:)` is not overridden.
 
 Each element is exposed as an IBOutlet so they can be customised by subclasses. For example a `UIViewController` showing PDFs might show a thumbnail of each page, rather than a title, with a custom selection indicator being a rectangle around the currently selected page. If a `pageControl` has been customised, `layoutSelectionIndicator()` should be overridden by the subclass to ensure the selection indicator appears in the correct place.
 
 In addition to replicating some of the `UIPageViewController`'s functionality, `JCPageViewController` provides the following extra features:
 
 - The `pageControl` property adds a default control, which can easily be customised by subclasses to provide user navigation to different pages.
 - Integrates with AutoLayout seemlessly so it can be added as a child view controller or subclassed to extend its functionality further, such as in `JCScrollingPageViewController`
 
 // TODO: Allow insets between pages and overlaps of left and right pages with the screen.
 
 // TODO: Allow cyclic navigation between pages.
 
 // TODO: Allow x pages per screen e.g. 2 pages in iPad landscape / iPad Pro
 
 The Page Control
 ----------------
 
 The `pageControl` property adds a `UIColletionView`, which can easily be customised by subclasses to provide user navigation to different pages. Selecting a tab scrolls to that page, and the selected page is underlined.
 
 The `pageControl` uses `JCPageControlCollectionViewFlowLayout` and `JCPageControlCell`s. This automatically calculates the size necessary to fit the `UIViewController`'s `title` on screen. This cell can easily be configured for multiple layouts which maintain this size calculation. This should be considered before implementing a custom cell and layout.
 
 The `selectionIndicator` `UIView` is positioned by responding to `pageContainer.contentOffset` in `scrollViewDidScroll(_:)`. Subclasses should ensure `super` is called if overriding this method.
 
 */
public class JCPageViewController: JCViewController {
    
    // MARK: - Properties
    
    private var needsUpdateViewControllersOnLoad = false
    
    /// All the UIViewControllers to be paged between. This is nil if a delegate is providing the child view controllers, otherwise it should contain all the `UIViewController`s to be displayed.
    public var viewControllers = [UIViewController]() {
        didSet {
            
            if !isViewLoaded() {
                needsUpdateViewControllersOnLoad = true
                return
            }
            
            if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
            
            let previousCount = needsUpdateViewControllersOnLoad ? 0 : oldValue.count
            needsUpdateViewControllersOnLoad = false
            
            // reset the pageContainers
            
            if previousCount < viewControllers.count {
                // remove the last container to clear its constraints
                
                if let pageN = viewContainers.last {
                    pageN.removeFromSuperview()
                }
                
                // create and add extra page containers (including the one just removed)
                
                let startIndex = max(oldValue.count - 1, 0)
                for p in startIndex..<(viewControllers.count) {
                    
                    // create the new page
                    let container = UIView(frame: self.view.bounds)
                    container.translatesAutoresizingMaskIntoConstraints = false
                    viewContainers.append(container)
                    
                    // update the constraints
                    if let pageContainer = self.pageContainer {
                        pageContainer.addSubview(container)
                        addConstraintsForPageNumber(p)
                    }
                }
                
            } else if previousCount > viewControllers.count {
                
                // if the old count was higher it has to have been non-nil
                let oldViewControllers = oldValue
                
                // remove the unnecessary page containers
                let startIndex = viewControllers.count
                for p in (startIndex)..<(oldViewControllers.count) {
                    let container = viewContainers[p]
                    container.removeFromSuperview()
                }
                
                viewContainers.removeRange((startIndex)...(oldViewControllers.count - 1))
                
                // update the last page constraint
                addConstraintsForPageNumber(viewContainers.count - 1)
            } else {
                // there are the same number of pages so the containers remain the same.
            }
            
            // make sure the page control has the correct number of tabs
            pageControl?.reloadData()
            currentViewControllerIndex = viewControllers.count > 0 ? 0 : nil
            updatePages()
            layoutSelectionIndicator()
            
            // return to the first page
            setSelectedViewController(0, animated: true)
        }
    }
    
    /// The UIScrollView to handle user interaction. The {@code bounds} property is observed so that the current page can be maintained through `handlePageContainerSizeChangeToSize(_:) @link.
    @IBOutlet public var pageContainer:UIScrollView?
    
    /// The collection view to use as an extra navigation tool. This could be a simple tab control similar to the Android viewPager or could be a thumbnail view controller similar to a pdf library.
    @IBOutlet public var pageControl:UICollectionView?
    
    /// The height of the `pageControl`. This is enforced using `pageControlHeightConstraint`. Setting this updates the constant of pageControlHeightConstraint and re-lays out the pageControl. If using the default layout, this actually determines the height of the items in the pageControl and the height and inset of the control is adjusted according to the topLayoutGuide.
    public var pageControlHeight:CGFloat {
        didSet {
            if pageControlHeight != oldValue {
                adjustPageControlConstraint()
                layoutSelectionIndicator()
                view.layoutSubviews()
            }
        }
    }
    
    /// This enforces the `pageControlHeight`. If using the default layout this may not be the actual value of pageControlHeight is adjusted according to the topLayoutGuide.
    @IBOutlet public var pageControlHeightConstraint:NSLayoutConstraint?
    
    /// This determines whether or not the default `pageControl` should be created and added to the view hierarchy. It should be set before viewDidLoad is called, eg when the view controller is instantiated from a storyboard or by setting it in the Storyboard. Setting it after viewDidLoad will have no effect.
    @IBInspectable public var showsPageControl:Bool = true
    
    /// The default indicator, a small underline on the pageControl for the selected page.
    public var selectionIndicator:UIView?
    
    /// This determines whether or not the default `selectionIndicator` should be created and added to the view hierarchy. It should be set before viewDidLoad is called, eg when the view controller is instantiated from a storyboard or by setting it in the Storyboard. Setting it after viewDidLoad will have no effect.
    @IBInspectable public var showsSelectionIndicator:Bool = true
    
    /// The delegate of this JCPageViewController.
    @IBOutlet public var delegate:JCPageViewControllerDelegate?
    
    // MARK: Read Only Properties
    
    /// The current UIViewController being displayed. This is readonly as only this class should manage how the visible view controllers are tracked. When set in `updatePages()`, this becomes a child view controller of {@code self}.
    private(set) public var currentViewController:UIViewController?
    
    /// The UIViewController to the left of the one being displayed. This is readonly as only this class should manage how the visible view controllers are tracked. When set in `updatePages()`, this becomes a child view controller of {@code self}.
    private(set) public var previousViewController:UIViewController?
    
    /// The UIViewController to the right of the one being displayed. This is readonly as only this class should manage how the visible view controllers are tracked. When set in `updatePages()`, this becomes a child view controller of {@code self}.
    private(set) public var nextViewController:UIViewController?
    
    /// The index in `viewControllers` of the view controller currently being displayed. This is readonly as only this class should manage how the visible view controllers are tracked. setSelectedViewControler(animated:) should be used externally to control page display. If `viewControllers` is empty, this should be nil.
    private(set) public var currentViewControllerIndex:Int?
    
    // MARK: Private Variables
    
    /// A collection of UIViews each of which will contain the view of a UIViewController in `viewControllers`. The view is added as a subview of this view dynamically as the `pageContainer` scrolls. This is private as it involves the internal workings of the class and could change.
    private var viewContainers:Array<UIView>
    
    /// Flag to determine whether the `pageContiainer` and other standard views should be assumed to be laid out in the default way. This should be set by subclasses only and should not be set externally. If a subclasses wishes to maintain certain behaviour but with a custom layout they should set this to false before calling super.viewDidLoad() and implement their own constraint and layout logic.
    @IBInspectable var usesDefaultLayout = true
    
    /// Flag to determine whether the constraints have been added. They should only need to be added once. If the number of view controllers in `viewControllers` changes, the constraints on the pages are adjusted then, not in updateViewConstraints()
    private var constraintsAdded = false
    
    /// Flag to check if the view is rotating, in which case the `pageContainer pageContainer's` delegate should respond to scrolling messages differently, to ensure the correct page is still selected after rotation.
    private var isRotating = false
    
    /// Flag to check if the `currentViewController` is being set to a non-adjacent page (usisng `setSelectedViewController(_:animated:)), in which case the `pageContainer pageContainer's` delegate should respond to scrolling messages differently, to ensure views aren't unnecessarily added to the hierarchy if they are between the view controllers.
    private var isJumping = false
    
    // MARK: - Initializers
    
    required public init?(coder aDecoder: NSCoder) {
        
        // use an initialiser to set the default values to allow didSet to be overriden by subclasses.
        pageControlHeight = 44.0
        viewContainers = []
        
        super.init(coder: aDecoder)
    }
    
    // MARK: - Observation Methods
    
    override public func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        
        // maintain the selected page on bounds change otherwise the contentOffset may be set incorrectly resulting in a page only being half visible.
        if (object as? UIScrollView) === pageContainer  && keyPath == "bounds",
            let oldSize = (change?[NSKeyValueChangeOldKey] as? NSValue)?.CGRectValue().size,
            let newSize = (change?[NSKeyValueChangeNewKey] as? NSValue)?.CGRectValue().size {
                
                if oldSize.width != newSize.width {
                    if debug_layout { print("Changing pageContainer from \(oldSize) to \(newSize)")}
                    handlePageContainerSizeChangeToSize(newSize)
                }
                
        }
    }
    
    deinit {
        // remove the observers
        NSNotificationCenter.defaultCenter().removeObserver(self)
        pageContainer?.removeObserver(self, forKeyPath: "bounds")
    }
    
    // MARK: - View Lifecycle
    
    override public func viewDidLoad() {
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if (pageContainer == nil) {
            // create a default container if none has been specified in a Storyboard
            pageContainer = UIScrollView(frame: self.view.bounds)
            pageContainer?.translatesAutoresizingMaskIntoConstraints = false
        } else {
            usesDefaultLayout = false
        }
        
        if showsPageControl {
            
            // create and configure the layout of the default page control
            let layout = JCPageControlCollectionViewFlowLayout()
            layout.itemSize = CGSizeMake(100, pageControlHeight)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            layout.scrollDirection = .Horizontal
            
            // create the default page control
            let controlFrame = CGRectMake(0, 0, self.view.bounds.size.width, pageControlHeight)
            
            let control:UICollectionView
            if let pageControl = self.pageControl {
                control = pageControl
                control.collectionViewLayout = layout
            } else {
                control  =  UICollectionView(frame: controlFrame, collectionViewLayout: layout)
                control.translatesAutoresizingMaskIntoConstraints = false
                control.backgroundColor = UIColor.whiteColor()
            }
            
            control.registerClass(JCPageControlCell.self, forCellWithReuseIdentifier: kDefaultPageControlCellIdentifier)
            
            control.showsHorizontalScrollIndicator = false
            control.showsVerticalScrollIndicator = false
            
            control.dataSource = self
            control.delegate = self
            
            pageControl = control
        }
        
        if let pageControl = self.pageControl {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "pageHeightUpdated:", name: JCPageControlHeightChanged, object: pageControl.collectionViewLayout)
        }
        
        if showsSelectionIndicator {
            // The frame for the indicator is calculated and set incrementally in scrollViewDidScroll so it should not have constraints associated with it, hence its translatesAutoresizingMaskIntoConstraints property is left as false
            
            let indicator:UIView
            
            if let id = selectionIndicator {
                indicator = id
            } else {
                indicator = UIView(frame: CGRectMake(0, 0, 0, 4))
                indicator.backgroundColor = UIColor.blackColor()
            }
            
            indicator.autoresizingMask = [.FlexibleTopMargin, .FlexibleWidth]
            selectionIndicator = indicator
        }
        
        // configure the paging container to its default values
        pageContainer?.pagingEnabled = true
        pageContainer?.scrollEnabled = true
        
        pageContainer?.bounces = true
        pageContainer?.alwaysBounceHorizontal = true
        pageContainer?.alwaysBounceVertical = false
        
        pageContainer?.showsHorizontalScrollIndicator = false
        pageContainer?.showsVerticalScrollIndicator = false
        
        pageContainer?.delegate = self
        pageContainer?.addObserver(self, forKeyPath: "bounds", options: [.Old, .New], context: nil)
        
        if let pageContainer = self.pageContainer where pageContainer.superview == nil {
            // create the view hierarchy separate from view creation so subclasses can use the objects configured in the default way but laid out differently.
            view.addSubview(pageContainer)
        }
        
        if let pageControl = self.pageControl where pageControl.superview == nil{
            view.addSubview(pageControl)
        }
        
        if let selectionIndicator = self.selectionIndicator where selectionIndicator.superview == nil {
            pageControl?.addSubview(selectionIndicator)
            pageControl?.bringSubviewToFront(selectionIndicator)
            selectionIndicator.layer.zPosition = 100
        }
        
        if needsUpdateViewControllersOnLoad {
            let vcs = viewControllers
            self.viewControllers = vcs
        }
        
        if debug_colours {
            pageControl?.backgroundColor = UIColor.brownColor()
            selectionIndicator?.backgroundColor = UIColor.redColor()
        }
    }

    override public func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
    }
    
    override public func viewDidAppear(animated: Bool) {
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        super.viewDidAppear(animated)
        
        //print("page guides: top:\(topLayoutGuide.length) bottom: \(bottomLayoutGuide.length)")
    }
    
    override public func viewDidLayoutSubviews() {
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        super.viewDidLayoutSubviews()
        
        if willAppear && !didAppear {
            resetChildLayoutGuides()
        }
        
        if usesDefaultLayout && showsPageControl {
            if let pageControl = self.pageControl {
                // adjust the page control insets so the content begins from the top layout guide, but the pageControl view can begin from the top of the view controller's view.
                if pageControl.contentInset.top != topLayoutGuide.length {
                    var insets = pageControl.contentInset
                    insets.top = topLayoutGuide.length
                    pageControl.contentInset = insets
                    view.layoutIfNeeded()
                }
            }
        }
        
        layoutSelectionIndicator()
    }
    
    /// Determines the default layout guides for child view controllers. If `usesDefaultLayout` is true, these are set on the `previousViewController`, `currentViewController` and `nextViewController`. If `usesDefaultLayout` is false it is the sub-class' responsibility to adjust the layout guides accordingly.
    public func resetChildLayoutGuides() {
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        var childLayoutGuides = UIEdgeInsetsMake(topLayoutGuide.length, 0, bottomLayoutGuide.length, 0)
        if showsPageControl {
            childLayoutGuides.top = 0
        }
        
        if usesDefaultLayout {
            adjustChildLayoutGuides(childLayoutGuides)
        }
    }
    
    /**
     
     Sets the topLayoutGuide.length and bottomLayoutGuide.length properties of the previous, current and next child view controllers.
     
     - parameter childLayoutGuides: The top and bottom properties are the values of the top and bottom layout guide lengths.
     */
    public func adjustChildLayoutGuides(childLayoutGuides: UIEdgeInsets) {
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        for childVC in [previousViewController, currentViewController, nextViewController] {
            if let vc = childVC {
                if vc.topLayoutGuide.length != childLayoutGuides.top || vc.bottomLayoutGuide.length != childLayoutGuides.bottom {
                    vc.setLayoutConstraintLengths(childLayoutGuides)
                    vc.view.layoutIfNeeded()
                }
            }
        }
    }
    
    // MARK: - Paging Methods
    
    /**
    Scrolls to the view controller at the specified index. This should be called after `viewControllers @\link has been set. It can be called before the view is displayed on screen to pre-select a page.
    
    - parameter index: The index of the view controller in `viewControllers` to scroll to.
    - parameter animated: Flag for whether the scroll should be animated. Defaults to true.
    */
    public func setSelectedViewController(viewControllerIndex : Int?, animated : Bool = true) {
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        guard let index = viewControllerIndex else {
                currentViewControllerIndex = nil
                return
        }
        
        if index < viewControllers.count {
            
            if !(willAppear || didAppear) {
                // store the chosen index as this is used in state restoration in viewDidLayoutSubviews (willAppear && !didAppear) so this method can be called before the view has been created.
                currentViewControllerIndex = index
                // updatePages() to ensure the correct pages are in the view hierarchy when the view appears.
                updatePages()
            } else {
                
                if index < viewContainers.count {
                    
                    if let currentIndex = currentViewControllerIndex {
                        // jumping is required for pre-selection to ensure the scrollViewDidScroll(_:) delegate methods correctly sets the content offset
                        isJumping = (index > currentIndex + 1) || (index < currentIndex - 1) || (willAppear && !didAppear)
                    } else {
                        isJumping = true
                    }
                    
                    if isJumping {
                        // set the currentViewControllerIndex as the goal index
                        currentViewControllerIndex = index
                    }
                    
                    let selectedPageContainer = viewContainers[index]
                    // scroll to the selected view controller if it has been created, animating if requested and the view has appeared, otherwise the animation has no effect
                    pageContainer?.setContentOffset(selectedPageContainer.frame.origin, animated: animated && didAppear)
                }
            }
        }
    }
    
    /**
     Helper method to scroll to the next page.
     */
    public func nextPage(animated: Bool = true) {
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        setSelectedViewController(currentViewControllerIndex ?? 0 + 1, animated: animated)
    }
    
    /**
     Helper method to scroll to the previous page. {@code null}
     */
    public func previousPage(animated: Bool = true) {
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        setSelectedViewController(currentViewControllerIndex ?? 0 - 1, animated: animated)
    }
    
    /**
     Configures the visible pages, managing which views should be added to, or removed from, their containers in `viewContainers @\link. Necessary UIViewController containment methods are also called here. Care should be taken when modifying this method so that views are not loaded prematurely for view controllers not yet in the view hierarchy. For performance issues this should only be called when the previous/current/nextViewController properties will change.
     */
    public func updatePages() {
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        guard let currentIndex = currentViewControllerIndex else { return }
        
            if currentViewControllerIndex < viewContainers.count {
                
                let prevIndex = currentIndex - 1
                let thisIndex = currentIndex
                let nextIndex = currentIndex + 1
                
                // clear any unavailable convenience accessor properties
                if thisIndex == 0 {
                    if debug_paging { print("no previous view controller") }
                    previousViewController = nil;
                }
                
                if thisIndex == viewControllers.count - 1 {
                    if debug_paging { print("no next view controller") }
                    nextViewController = nil;
                }
                
                // iterate through the view controllers adjusting the view hierarchy as necessary
                for (p, thisViewController) in viewControllers.enumerate() {
                    
                    // set the convenience accessor properties
                    if p == prevIndex {
                        if debug_paging { print("setting previous view controller \(p)") }
                        previousViewController = thisViewController
                    }
                    
                    if p == thisIndex {
                        if debug_paging { print("setting current view controller \(p)") }
                        
                        // update the pageControl's selectedItem property if necessary
                        if currentViewController !== thisViewController {
                            currentViewController = thisViewController
                            delegate?.pageViewController?(self, didChangeCurrentPageTo: thisViewController, atIndex: p)
                            
                            if let pageControl = self.pageControl {
                                pageControl.selectItemAtIndexPath(NSIndexPath(forItem: p, inSection: 0), animated: true, scrollPosition: .CenteredHorizontally)
                            }
                        }
                    }
                    
                    if p == nextIndex {
                        if debug_paging { print("setting next view controller \(p)") }
                        nextViewController = thisViewController
                    }
                    
                    // manage the view hierarchy
                    let container = viewContainers[p]
                    if (p < prevIndex || p > nextIndex) {
                        
                        if thisViewController.view.superview != nil {
                            // any existing view that is no longer needed should be removed
                            let description = (p < prevIndex) ? "previous" : "next"
                            if debug_paging { print("removing old \(description) view controller \(p)") }
                            
                            // transition from the view hierarchy sending delegate and container methods
                            delegate?.pageViewController?(self, willRemovePage: thisViewController, fromIndex: p)
                            thisViewController.willMoveToParentViewController(nil)
                            thisViewController.view.removeFromSuperview()
                            thisViewController.removeFromParentViewController()
                            delegate?.pageViewController?(self, didRemovePage: thisViewController, fromIndex: p)
                        }
                    } else {
                        // the view should be added as it is previous / current / next
                        let page = thisViewController.view
                        
                        if page.superview == nil {
                            let constraints = container.constraintsToAlignView(page)
                            
                            if debug_paging { print("added page \(p)") }
                            
                            // before adding the view to the hierarchy ensure it will not attempt any auto-resizing
                            page.translatesAutoresizingMaskIntoConstraints = false
                            
                            // transition to the view hierarchy sending delegate and container methods
                            delegate?.pageViewController?(self, willAddPage: thisViewController, atIndex: p)
                            self.addChildViewController(thisViewController)
                            
                            if willAppear && !didAppear {
                                // `viewWillAppear(_:)` will not be forwarded to the child vc so manually call it here. `viewDidAppear(_:)` will get called below
                                thisViewController.beginAppearanceTransition(true, animated: false)
                            }
                            
                            container.addSubview(page)
                            container.addConstraints(constraints)
                            thisViewController.didMoveToParentViewController(self)
                            
                            if willAppear && didAppear {
                                // `viewWillAppear(_:)` will not be forwarded to the child vc so manually call it here. `viewDidAppear(_:)` will get called below
                                thisViewController.endAppearanceTransition()
                            }
                            
                            delegate?.pageViewController?(self, didAddPage: thisViewController, atIndex: p)
                        }
                    }
                }
                
                resetChildLayoutGuides()
            }
    }
    
    // MARK: - Layout Methods
    
    public func pageHeightUpdated(note:NSNotification) {
        if let layout = note.object as? JCPageControlCollectionViewFlowLayout {
            pageControlHeight = layout.maxHeight
        }
    }
    
    override public func updateViewConstraints() {
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        super.updateViewConstraints()
        
        // the scroll view's constraints remain the same throughout the view controller's lifetime. Each page's constraints are managed when the viewController's property is set.
        if !constraintsAdded {
            
            if let pageContainer = self.pageContainer {
                
                var vDict:[String:AnyObject] = ["scroll":pageContainer, "topLayoutGuide": topLayoutGuide]
                
                if (usesDefaultLayout)  {
                    // ensure the scroll container fills this view if using the default layout
                    
                    self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[scroll]|",
                        options:.DirectionLeftToRight,
                        metrics: nil,
                        views: vDict))
                    
                    if let pageControl = self.pageControl {
                        
                        // if showing the pageControl construct the default layout as a tab bar at the top of the page, mimicing the Android ViewPager
                        vDict["control"] = pageControl
                        
                        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[control]|",
                            options:.DirectionLeftToRight,
                            metrics: nil,
                            views: vDict))
                        
                        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[control][scroll]|",
                            options:.DirectionLeftToRight,
                            metrics: nil,
                            views: vDict))
                        
                        self.view.addConstraint(NSLayoutConstraint(item: pageControl,
                            attribute: .Top,
                            relatedBy: .Equal,
                            toItem: view,
                            attribute: .Top,
                            multiplier: 1.0,
                            constant: 0.0))
                        
                    } else {
                        // default layout is the scroll view taking up the whole screen
                        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[scroll]|",
                            options:.DirectionLeftToRight,
                            metrics: nil,
                            views: vDict))
                    }
                    
                    constraintsAdded = true
                }
                
                if let pageControl = self.pageControl {
                    if pageControlHeightConstraint == nil {
                        let constr = NSLayoutConstraint(item: pageControl,
                            attribute: .Height,
                            relatedBy: .Equal,
                            toItem: nil,
                            attribute: .NotAnAttribute,
                            multiplier: 0.0,
                            constant: pageControlHeight)
                        pageControl.addConstraint(constr)
                        pageControlHeightConstraint = constr
                    }
                }
                
                adjustPageControlConstraint()
            }
        }
    }
    
    /// Updates the `pageControlHeightConstraint` to match the `pageControlHeight` taking topLayoutGuide into account if necessary.
    public func adjustPageControlConstraint() {
        
        var pcHeight = pageControlHeight
        
        if usesDefaultLayout {
            pcHeight += topLayoutGuide.length
        }
        
        if let pageControlHeightConstraint = self.pageControlHeightConstraint {
            pageControlHeightConstraint.constant = pcHeight
        }
    }
    
    /**
     
     Adds constraints to the `pageContainer @\link to align the container views in `viewContainers` correctly. They are aligned to be the same height and width as pageContainer and form a single flush horizontal row of containers.
     
     - parameter pageNumber: The index of the page container in viewContainers for which constraints should be added.
     */
    public func addConstraintsForPageNumber(pageNumber:Int) {
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        if let pageContainer = self.pageContainer {
            
                if pageNumber < viewControllers.count {
                    if pageNumber == 0 {
                        
                        // add the horizontal constraints for the first container
                        if let page0 = viewContainers.first {
                            pageContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[page]",
                                options: .DirectionLeftToRight,
                                metrics: nil,
                                views: ["page":page0]))
                        }
                    }
                    
                    // page could be the first and last page if there is only 1 page
                    if pageNumber == (viewControllers.count - 1) {
                        
                        // add the horizontal constraints for the last container
                        if let pageN = viewContainers.last {
                            pageContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("[page]|",
                                options: .DirectionLeftToRight,
                                metrics: nil,
                                views: ["page":pageN]))
                        }
                    }
                    
                    if pageNumber > 0 {
                        
                        // add the horizontal constraints for the chaining containers
                        let prevPage = viewContainers[pageNumber-1]
                        let thisPage = viewContainers[pageNumber];
                        
                        pageContainer.addConstraint(NSLayoutConstraint(item: thisPage,
                            attribute: .Left,
                            relatedBy: .Equal,
                            toItem: prevPage,
                            attribute: .Right,
                            multiplier: 1.0,
                            constant: 0.0))
                    }
                    
                    // add the width, height & vertical constraints for all the containers
                    let page = viewContainers[pageNumber]
                    let pDict:[String: AnyObject] = ["page":page]
                    
                    pageContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[page]|",
                        options: .DirectionLeftToRight,
                        metrics: nil,
                        views: pDict))
                    
                    pageContainer.addConstraint(NSLayoutConstraint(item: page,
                        attribute: .Width,
                        relatedBy: .Equal,
                        toItem: pageContainer,
                        attribute: .Width,
                        multiplier: 1.0,
                        constant: 0.0))
                    
                    pageContainer.addConstraint(NSLayoutConstraint(item: page,
                        attribute: .Height,
                        relatedBy: .Equal,
                        toItem: pageContainer,
                        attribute: .Height,
                        multiplier: 1.0,
                        constant: 0.0))
                }
        }
    }
    
    /**
     Sets the frame of the `selectionIndicator` property based on the proportion of views visible on screen. Subclass can override this method to customise the position of the indicator. Calling {@code super} will set the frame to its default value first, then single properties such as {@code height} or {@code origin.y} could be overridden.
     */
    public func layoutSelectionIndicator() {
        // layout the selection indicator
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        if let pageControl = self.pageControl,
            let selectionIndicator = self.selectionIndicator {
                
                // layout the selectionIndicator at the bottom of the cells representing the pages visible on screen
                let vBounds = view.bounds
                
                // calculate the indicator's frame based on the pageControl's tab's frames, using the same ratio as the page's frames
                var firstIndexPathWithOverlap:NSIndexPath? = nil
                var lastIndexPathWithOverlap:NSIndexPath? = nil
                
                var startX:CGFloat = 0.0
                var endX: CGFloat = 0.0
                
                for p in 0..<viewContainers.count {
                    let container = viewContainers[p]
                    let containerFrameInView = view.convertRect(container.frame, fromView: container.superview)
                    
                    let containerIntersection = CGRectIntersection(containerFrameInView, vBounds)
                    
                    if firstIndexPathWithOverlap == nil {
                        // check if there is an overlap with this page and self.view's bounds
                        if !CGRectIsNull(containerIntersection) && containerIntersection.size.width > 0 {
                            let path = NSIndexPath(forItem: p, inSection: 0)
                            firstIndexPathWithOverlap = path
                            // calculate and set the starting position of the indicator based on the overlap with the pageControl cell for this index path
                            let startProportion = (CGRectGetMinX(containerIntersection) - containerFrameInView.origin.x) / containerFrameInView.size.width
                            if let startCell = pageControl.cellForItemAtIndexPath(path) {
                                // let startCellFrame = view.convertRect(startCell.frame, fromView: startCell.superview)
                                startX = startCell.frame.origin.x + startProportion * startCell.frame.size.width
                            }
                        }
                    } else {
                        if lastIndexPathWithOverlap == nil {
                            // looking for the last intersection which is the first one before there is no intersection
                            if CGRectIsNull(containerIntersection) || containerIntersection.size.width == 0 {
                                lastIndexPathWithOverlap = NSIndexPath(forItem: p - 1, inSection: 0)
                            }
                        }
                    }
                }
                
                // if the last never got set it was the final page
                if lastIndexPathWithOverlap == nil && viewContainers.count > 0 {
                    lastIndexPathWithOverlap = NSIndexPath(forItem: viewContainers.count - 1, inSection: 0)
                }
                
                // based on the last path, which will now definitely be set, calculate the ending position
                if let path = lastIndexPathWithOverlap {
                    let container = viewContainers[path.row]
                    let containerFrameInView = view.convertRect(container.frame, fromView: container.superview)
                    
                    let containerIntersection = CGRectIntersection(containerFrameInView, vBounds)
                    
                    if !CGRectIsNull(containerIntersection) && containerIntersection.size.width > 0 {
                        // calculate and set the end position of the indicator based on the overlap with the pageControl cell for this index path
                        let endProportion = (CGRectGetMaxX(containerIntersection) - containerFrameInView.origin.x) / containerFrameInView.size.width
                        if let endCell = pageControl.cellForItemAtIndexPath(path) {
                            // let endCellFrame = view.convertRect(endCell.frame, fromView: endCell.superview)
                            endX = endCell.frame.origin.x + endProportion * endCell.frame.size.width
                        }
                    }
                }
                
                // now the visible page proportions are known, set the frame of the indicator
                var indicatorFrame = selectionIndicator.frame
                indicatorFrame.origin.y = pageControl.bounds.origin.y + pageControl.frame.size.height - indicatorFrame.size.height
                indicatorFrame.origin.x = startX
                
                if endX >= startX {
                    indicatorFrame.size.width = endX - startX
                } else {
                    indicatorFrame.size.width = 0
                    print("Error calculating selectionIndicator frame, got \(startX) - \(endX)")
                }
                
                selectionIndicator.frame = indicatorFrame
        } else {
            var currentFrame = selectionIndicator?.frame ?? CGRectZero
            currentFrame.size.width = 0
            selectionIndicator?.frame = currentFrame
        }
    }
    
    // Re-select the currentViewControllerIndex in the pageControl to ensure the cell is correctly centered.
    public func resetPageControlSelection() {
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        guard let currentIndex = currentViewControllerIndex else { return }
        
        pageControl?.selectItemAtIndexPath(NSIndexPath(forItem: currentIndex, inSection: 0), animated: didAppear, scrollPosition: .CenteredHorizontally)
    }
    
    // MARK: Rotation Methods
    
    /**
    In order to maintain the correct page on display when the `pageContainer pageContainer's@/link @{code bounds} change, this method is called to manually re-set the contentOffset to the correct position given the new sizes of each page's container.
    */
    public func handlePageContainerSizeChangeToSize(toSize: CGSize) {
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        isRotating = true
        
        if let pageContainer = self.pageContainer,
        let currentIndex = currentViewControllerIndex {
            // print("currentOffset: \(pageContainer.contentOffset.x)")
            let newWidth = toSize.width
            let newOffsetX = newWidth * CGFloat(currentIndex)
            // print("adjustedOffset: \(newOffsetX)")
            
            if pageContainer.contentOffset.x != newOffsetX {
                pageContainer.contentOffset = CGPointMake(newOffsetX, 0)
                
                resetPageControlSelection()
                layoutSelectionIndicator()
            }
        }
        
        isRotating = false
    }

    override public func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        isRotating = true
        
        coordinator.animateAlongsideTransition({ (_) -> Void in
            }) { (_) -> Void in
                self.resetPageControlSelection()
        }
    }
}

// MARK: - Scroll View Delegate

extension JCPageViewController : UIScrollViewDelegate {
    
    public func scrollViewDidScroll(scrollView: UIScrollView) {
        
        //        if print_methods { println("\(self.debugDescription).\(__FUNCTION__)") }
        
        if scrollView === pageContainer {
            
            // calculate whether we are on to a new page
            if !isRotating {
                // The content offset changes when the view rotates but the page should be maintained using handlePageContainerSizeChangeToSize(toSize: CGSize) method so do not update it here
                let scrollWidth = scrollView.frame.size.width
                let scrollCenterX = scrollView.contentOffset.x + scrollWidth / 2.0
                let proposedIndex = Int(scrollCenterX / scrollWidth)
                
                let maxIndex = (viewControllers.count) - 1
                let newIndex = max(min(proposedIndex, maxIndex), 0)
                
                if isJumping {
                    // check to see whether the newIndex is now the goal index
                    if newIndex == currentViewControllerIndex {
                        updatePages()
                        isJumping = false
                    }
                } else {
                    if currentViewControllerIndex != newIndex {
                        if debug_paging { print("selected page \(newIndex)") }
                        currentViewControllerIndex = newIndex
                        updatePages()
                        resetPageControlSelection()
                    }
                }
            }
            
            layoutSelectionIndicator()
        }
        
        if scrollView === pageControl {
            layoutSelectionIndicator()
        }
    }
}

// MARK: - Collection View Protocols

/**
The default `pageControl` is a UICollectionView. This extension implements the methods necessary to populate and manage that pageControl.
*/
extension JCPageViewController : UICollectionViewDataSource {
    
    public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewControllers.count
    }
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(kDefaultPageControlCellIdentifier, forIndexPath: indexPath)
        
        if let pageCell = cell as? JCPageControlCell,
        let delegate = collectionView.delegate as? UICollectionViewDelegatePageControlLayout {
            pageCell.titleLabel.text = delegate.collectionView(collectionView, layout: collectionView.collectionViewLayout, titleForIndexPath: indexPath)
            pageCell.subtitleLabel.text = delegate.collectionView(collectionView, layout: collectionView.collectionViewLayout, subtitleForIndexPath: indexPath)
            pageCell.imageView.image = delegate.collectionView(collectionView, layout: collectionView.collectionViewLayout, imageForIndexPath: indexPath)
            
            pageCell.titleLabel.font = delegate.collectionView(collectionView, layout: collectionView.collectionViewLayout, titleFontForIndexPath: indexPath)
            pageCell.subtitleLabel.font = delegate.collectionView(collectionView, layout: collectionView.collectionViewLayout, subtitleFontForIndexPath: indexPath)
            
            pageCell.titleLabel.hidden = pageCell.titleLabel.text == nil
            pageCell.subtitleLabel.hidden = pageCell.subtitleLabel.text == nil
            pageCell.imageView.hidden = pageCell.imageView.image == nil
        }
        
        return cell;
    }
}

extension JCPageViewController : UICollectionViewDelegate {
    
    // if the collection view adjusts the layout of its tabs, the selectionIndicator should be updated accordingly.
    public func collectionViewDidLayoutSubviews(collectionView: UICollectionView) {
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        if collectionView === pageControl {
            layoutSelectionIndicator()
        }
    }
    
    public func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if print_methods { print("\(self.debugDescription).\(__FUNCTION__)") }
        
        if collectionView === pageControl {
            setSelectedViewController(indexPath.item, animated: true)
        }
    }
    
}

extension JCPageViewController : UICollectionViewDelegatePageControlLayout {
    
    // Default Size Implementation
    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let size = (collectionViewLayout as? JCPageControlCollectionViewFlowLayout)?.cellSizes[indexPath] ?? DefaultTabSize

        return size
    }
    
    // return default values
    public func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, titleForIndexPath indexPath: NSIndexPath) -> String? {
        return viewControllers[indexPath.item].title
    }
    
    public func collectionView(collectionView:UICollectionView, layout:UICollectionViewLayout, titleFontForIndexPath indexPath:NSIndexPath) -> UIFont? {
        return UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline)
    }
    
    public func collectionView(collectionView:UICollectionView, layout:UICollectionViewLayout, subtitleForIndexPath indexPath:NSIndexPath) -> String? {
        return nil
    }
    
    public func collectionView(collectionView:UICollectionView, layout:UICollectionViewLayout, subtitleFontForIndexPath indexPath:NSIndexPath) -> UIFont? {
        return UIFont.preferredFontForTextStyle(UIFontTextStyleCaption1)
    }
    
    public func collectionView(collectionView:UICollectionView, layout:UICollectionViewLayout, imageForIndexPath indexPath:NSIndexPath) -> UIImage? {
        return nil
    }
    
    public func collectionView(collectionView:UICollectionView, layout:UICollectionViewLayout, stackInsetsForIndexPath indexPath:NSIndexPath) -> UIEdgeInsets {
        return JCPageControlCell.stackInsets
    }
    
    public func collectionView(collectionView:UICollectionView, layout:UICollectionViewLayout, textSpacingForIndexPath indexPath:NSIndexPath) -> CGFloat {
        return JCPageControlCell.textSpacing
    }
    
    public func collectionView(collectionView:UICollectionView, layout:UICollectionViewLayout, stackSpacingForIndexPath indexPath:NSIndexPath) -> CGFloat {
        return JCPageControlCell.stackSpacing
    }
}