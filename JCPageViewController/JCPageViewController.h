//
//  JCPageViewController.h
//  JCPageViewController
//
//  Created by Josh Campion on 06/12/2015.
//  Copyright © 2015 Josh Campion Development. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for JCPageViewController.
FOUNDATION_EXPORT double JCPageViewControllerVersionNumber;

//! Project version string for JCPageViewController.
FOUNDATION_EXPORT const unsigned char JCPageViewControllerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <JCPageViewController/PublicHeader.h>


